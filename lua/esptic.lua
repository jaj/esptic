clientid = "ESPTIC v0.3"

STX = '\x02'
ETX = '\x03'
topic_root = 'home/power/tic/'

ticlabels = {
    BASE  = 'energy',
    HCHC  = 'energy_hc',
    HCHP  = 'energy_hp',
    PTEC  = 'period',
    IINST = 'current',
    PAPP  = 'power'
}

print(clientid)

conntimer = tmr.create()
connected = false

bufstack = {}

mqttclient = mqtt.Client(clientid, 120)

function debugprint(msg)
    mqttclient:publish(topic_root..'debug', msg, 0, 0)
end

function publishData(key, value)
    key = ticlabels[key]
    if key == nil then
        return
    end
    if key == 'period' then
        if string.find(value, "HC") ~= nil then
            value = 1
        else
            value = 0
        end
    else
        value = tonumber(value)
    end
    topic = topic_root..key
    mqttclient:publish(topic, value, 0, 0)
end

function calcChk(data)
    sum = 0
    bytes = {string.byte(data, 1, -1)}
    for _, v in ipairs(bytes) do
        sum = sum + v
    end
    return bit.band(sum, 63) + 32
end

function processbuffer()
    buffer = table.concat(bufstack, "")
    --debugprint('buffer '..buffer)
    bufstack = {}
    lim = string.find(buffer, "\r\n")
    if lim == nil then
        return false
    end
    buffer = string.gsub(buffer, ETX..STX, "")
    data = string.sub(buffer, 1, lim - 1)
    -- Put remainder back on the stack
    rem = string.sub(buffer, lim + 2)
    --debugprint('data '..data)
    --debugprint('rem '..rem)
    table.insert(bufstack, rem)

    msg = string.sub(data, 1, -3)
    chk = string.byte(data, -1)
    mychk = calcChk(msg)
    --debugprint('chk '..chk..' '..mychk)
    if mychk ~= chk then
        return true
    end

    delim = string.find(msg, " ")
    if delim == nil then
        return true
    end
    key = string.sub(msg, 1, delim - 1)
    val = string.sub(msg, delim + 1)
    --debugprint('kv '..key..' '..val)
    publishData(key, val)

    return true
end

function fillbuffer(data)
    table.insert(bufstack, data)
    if string.find(data, "\n") ~= nil then
        processbuffer()
        --while processbuffer() do end
    end
end

function connectgateway(T)
    if connected then
        return
    end

    print("Trying to connect to mqtt on gateway " .. gateway)

    mqttclient:lwt(topic_root..'status', "offline", 0, 0)
    mqttclient:connect(gateway)

    mqttclient:on("connect", function(client)
        connected = true
        mqttclient:publish(topic_root..'status', 'online', 0, 0)
        print("Connected")
        uart.setup(0, 1200, 7, uart.PARITY_EVEN, uart.STOPBITS_1, 1)
        uart.on("data", 0, fillbuffer, 0)
    end)

    mqttclient:on("offline", function(client)
        mqttclient:close()
        connected = false
        uart.on("data")
        uart.setup(0, 115200, 8, 0, 1, 1)
        print("Disconnected. UART reverted")
    end)
end

function wifiup(T)
    ip, netmask, gateway = wifi.sta.getip()
    print("Got IP " .. ip)

    conntimer:register(5000, tmr.ALARM_AUTO, connectgateway)
    conntimer:start()
end


print("Starting WIFI")
wifi.setmode(wifi.STATION)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifiup)
wifi.sta.config(cfg)
