-- init.lua --

uart.setup(0, 115200, 8, 0, 1, 1)

print("MAC Address: " .. wifi.sta.getmac())
print("Chip ID: " .. node.chipid())
print("Flash ID: " .. node.flashid())
print("Heap Size: " .. node.heap())

dofile("cred.lua")
dofile("esptic.lua")
