from typing import List, Tuple, Optional

import socket
import threading
from queue import Queue

import paho.mqtt.client as mqtt

STX = b'\x02'
ETX = b'\x03'
LOCALHOST = '0.0.0.0'
ESPTICPORT = 9876
Message = Tuple[str, int]

ticlabels = {
    b'BASE' : 'energy',
    b'HCHC' : 'energy_hc',
    b'HCHP' : 'energy_hp',
    b'PTEC' : 'period',
    b'IINST' : 'current',
    b'PAPP' : 'power'
}


recvq: 'Queue[bytes]' = Queue()
mesgq: 'Queue[Message]' = Queue()

mqttclient = mqtt.Client('espticproxy')
mqttclient.connect('127.0.0.1', 1883, 60)

def calcChk(msg: bytes):
    return (sum(msg) & 0x3F) + 0x20

def qhandler(recvq, mesgq):
    buffer = b''
    while True:
        for b in recvq.get():
            if type(b) == int:
                b = bytes([b])

            if b in [STX, ETX]:
                pass
            elif b == b'\n':
                if len(buffer) < 2:
                    continue
                msg = parseFrame(buffer[:-1])
                buffer = b''
                if msg is None:
                    continue
                mesgq.put(msg)
            else:
                buffer += b

def parseFrame(frame: bytes) -> Optional[Message]:
    msg = frame[:-2]
    chk = frame[-1]
    mychk = calcChk(msg)
    ok = (mychk == chk)
    splt = msg.split(b' ', 1)
    if len(splt) < 2:
        return None
    key, value = splt
    if not ok or key not in ticlabels:
        return None
    skey = ticlabels[key]
    svalue = value.decode('latin1')
    if skey == 'period':
        ivalue = int(svalue.startswith('HC'))
    else:
        try:
            ivalue = int(svalue)
        except ValueError:
            return None

    return (skey, ivalue)

def listener(sock, q: 'Queue[bytes]'):
    while True:
        data = sock.recv(1024)
        q.put(data)

def publisher(mqttclient, q: 'Queue[Message]'):
    topic_root = 'home/power/tic/'
    while True:
        try:
          topic, payload = q.get()
        except:
          continue
        print('.', end ="")
        #print(f'topic: {topic}, payload: {payload}')
        topic = topic_root + topic
        mqttclient.publish(topic, payload)

qhandler_t = threading.Thread(target=qhandler, args=[recvq, mesgq], daemon=True)
qhandler_t.start()

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((LOCALHOST, ESPTICPORT))
serversocket.listen(1)

mqtt_publisher = threading.Thread(target=publisher, args=[mqttclient, mesgq], daemon=True)
mqtt_publisher.start()

while True:
    (clientsocket, address) = serversocket.accept()
    print('got connection')
    esptic_listener = threading.Thread(target=listener, args=[clientsocket, recvq], daemon=True)
    esptic_listener.start()
