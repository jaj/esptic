
def calcChk(msg):
    return (sum(msg) & 0x3F) + 0x20


if __name__ == '__main__':
    truecount = 0
    falsecount = 0

    import sys 
    infile = sys.argv[1]
    f = open(infile, 'rb')
    data = f.read()
    frames = data.split(b'\x03\x02')
    for f in frames:
        messages = f.split(b'\r\n')
        for m in messages:
            m1 = m[:-2]
            chk = m[-1]
            mychk = calcChk(m1)
            ok = (mychk == chk)
            if ok:
                truecount += 1
            else:
                falsecount += 1
            key, value = m1.split(b' ', 1)
            print(f'<{m}> <{key}> <{value}> <{chk}> <{mychk}> <{ok}>')
    print(f'True: {truecount}, False: {falsecount}')
