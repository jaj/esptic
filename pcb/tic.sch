EESchema Schematic File Version 4
LIBS:tic-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5CA0D596
P 4150 3200
F 0 "R1" V 3943 3200 50  0000 C CNN
F 1 "4K7" V 4034 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4080 3200 50  0001 C CNN
F 3 "~" H 4150 3200 50  0001 C CNN
	1    4150 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CA0D779
P 5900 2600
F 0 "R3" H 5970 2646 50  0000 L CNN
F 1 "10K" H 5970 2555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5830 2600 50  0001 C CNN
F 3 "~" H 5900 2600 50  0001 C CNN
	1    5900 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5CA0DA5A
P 5300 3450
F 0 "R2" H 5370 3496 50  0000 L CNN
F 1 "10K" H 5370 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5230 3450 50  0001 C CNN
F 3 "~" H 5300 3450 50  0001 C CNN
	1    5300 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5CA0DAB9
P 5900 3550
F 0 "#PWR0101" H 5900 3300 50  0001 C CNN
F 1 "GND" H 5905 3377 50  0000 C CNN
F 2 "" H 5900 3550 50  0001 C CNN
F 3 "" H 5900 3550 50  0001 C CNN
	1    5900 3550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 5CA0DBE4
P 5450 2850
F 0 "#PWR0102" H 5450 2700 50  0001 C CNN
F 1 "+3.3V" H 5465 3023 50  0000 C CNN
F 2 "" H 5450 2850 50  0001 C CNN
F 3 "" H 5450 2850 50  0001 C CNN
	1    5450 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5CA0DF26
P 3200 3000
F 0 "J1" H 3306 3178 50  0000 C CNN
F 1 "TELEINFO" H 3306 3087 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3200 3000 50  0001 C CNN
F 3 "~" H 3200 3000 50  0001 C CNN
	1    3200 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0103
U 1 1 5CA0E328
P 5900 2250
F 0 "#PWR0103" H 5900 2100 50  0001 C CNN
F 1 "+3.3V" H 5915 2423 50  0000 C CNN
F 2 "" H 5900 2250 50  0001 C CNN
F 3 "" H 5900 2250 50  0001 C CNN
	1    5900 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5CA0E44A
P 5300 3750
F 0 "#PWR0104" H 5300 3500 50  0001 C CNN
F 1 "GND" H 5305 3577 50  0000 C CNN
F 2 "" H 5300 3750 50  0001 C CNN
F 3 "" H 5300 3750 50  0001 C CNN
	1    5300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3000 3750 3000
Wire Wire Line
	3750 3000 3750 2900
Wire Wire Line
	3400 3100 3750 3100
Wire Wire Line
	3750 3100 3750 3200
Wire Wire Line
	3750 3200 3950 3200
Wire Wire Line
	4700 3200 4700 3150
Wire Wire Line
	4700 2900 4700 2950
Wire Wire Line
	5300 2950 5450 2950
Wire Wire Line
	5450 2950 5450 2850
Wire Wire Line
	5300 3150 5300 3300
Wire Wire Line
	5300 3600 5300 3750
Wire Wire Line
	5300 3150 5600 3150
Wire Wire Line
	5900 2450 5900 2250
Wire Wire Line
	5900 3350 5900 3550
$Comp
L power:GND #PWR0105
U 1 1 5CA0F91F
P 8400 3250
F 0 "#PWR0105" H 8400 3000 50  0001 C CNN
F 1 "GND" H 8405 3077 50  0000 C CNN
F 2 "" H 8400 3250 50  0001 C CNN
F 3 "" H 8400 3250 50  0001 C CNN
	1    8400 3250
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LF120_TO220 U2
U 1 1 5CA0FD0B
P 4250 2000
F 0 "U2" H 4250 2242 50  0000 C CNN
F 1 "3v3 LDO" H 4250 2151 50  0000 C CNN
F 2 "usrlib:3v3LDO" H 4250 2225 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/c4/0e/7e/2a/be/bc/4c/bd/CD00000546.pdf/files/CD00000546.pdf/jcr:content/translations/en.CD00000546.pdf" H 4250 1950 50  0001 C CNN
	1    4250 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5CA1003E
P 3750 2000
F 0 "#PWR0106" H 3750 1850 50  0001 C CNN
F 1 "+5V" H 3765 2173 50  0000 C CNN
F 2 "" H 3750 2000 50  0001 C CNN
F 3 "" H 3750 2000 50  0001 C CNN
	1    3750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0107
U 1 1 5CA101BD
P 4750 2000
F 0 "#PWR0107" H 4750 1850 50  0001 C CNN
F 1 "+3.3V" H 4765 2173 50  0000 C CNN
F 2 "" H 4750 2000 50  0001 C CNN
F 3 "" H 4750 2000 50  0001 C CNN
	1    4750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5CA1024B
P 4250 2400
F 0 "#PWR0108" H 4250 2150 50  0001 C CNN
F 1 "GND" H 4255 2227 50  0000 C CNN
F 2 "" H 4250 2400 50  0001 C CNN
F 3 "" H 4250 2400 50  0001 C CNN
	1    4250 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0109
U 1 1 5CA10524
P 6500 2650
F 0 "#PWR0109" H 6500 2500 50  0001 C CNN
F 1 "+3.3V" H 6515 2823 50  0000 C CNN
F 2 "" H 6500 2650 50  0001 C CNN
F 3 "" H 6500 2650 50  0001 C CNN
	1    6500 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2000 4750 2000
Wire Wire Line
	3750 2000 3950 2000
Wire Wire Line
	4250 2400 4250 2300
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5CA10CF2
P 3000 4200
F 0 "J2" H 3106 4378 50  0000 C CNN
F 1 "POWER" H 3106 4287 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3000 4200 50  0001 C CNN
F 3 "~" H 3000 4200 50  0001 C CNN
	1    3000 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5CA10EC5
P 3700 4100
F 0 "#PWR0110" H 3700 3950 50  0001 C CNN
F 1 "+5V" H 3715 4273 50  0000 C CNN
F 2 "" H 3700 4100 50  0001 C CNN
F 3 "" H 3700 4100 50  0001 C CNN
	1    3700 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5CA10F0C
P 3700 4400
F 0 "#PWR0111" H 3700 4150 50  0001 C CNN
F 1 "GND" H 3705 4227 50  0000 C CNN
F 2 "" H 3700 4400 50  0001 C CNN
F 3 "" H 3700 4400 50  0001 C CNN
	1    3700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4200 3700 4200
Wire Wire Line
	3700 4200 3700 4100
Wire Wire Line
	3700 4300 3700 4400
Wire Wire Line
	3200 4300 3700 4300
Wire Wire Line
	5900 2750 5900 2850
$Comp
L tic-rescue:ESP8266-01_ESP-01-ESP8266-01_ESP-01 U3
U 1 1 5CA13E8F
P 7500 2950
F 0 "U3" H 7500 3617 50  0000 C CNN
F 1 "ESP8266-01_ESP-01" H 7500 3526 50  0000 C CNN
F 2 "ESP8266-01_ESP-01:XCVR_ESP8266-01%2fESP-01" H 7500 2950 50  0001 L BNN
F 3 "" H 7500 2950 50  0001 L BNN
F 4 "Unavailable" H 7500 2950 50  0001 L BNN "Field4"
F 5 "None" H 7500 2950 50  0001 L BNN "Field5"
F 6 "None" H 7500 2950 50  0001 L BNN "Field6"
F 7 "AI-Thinker" H 7500 2950 50  0001 L BNN "Field7"
F 8 "ESP8266-01/ESP-01" H 7500 2950 50  0001 L BNN "Field8"
	1    7500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2850 6500 2850
Wire Wire Line
	6500 2850 6500 2950
Wire Wire Line
	6500 2950 6800 2950
Connection ~ 5900 2850
Wire Wire Line
	5900 2850 5900 2950
Wire Wire Line
	6500 2650 6800 2650
$Comp
L power:+3.3V #PWR0112
U 1 1 5CA14610
P 8400 2550
F 0 "#PWR0112" H 8400 2400 50  0001 C CNN
F 1 "+3.3V" H 8415 2723 50  0000 C CNN
F 2 "" H 8400 2550 50  0001 C CNN
F 3 "" H 8400 2550 50  0001 C CNN
	1    8400 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2550 8400 2550
Wire Wire Line
	8200 3250 8400 3250
NoConn ~ 8200 2950
NoConn ~ 6800 3250
NoConn ~ 6800 3150
NoConn ~ 6800 2550
$Comp
L Transistor_FET:2N7000 Q1
U 1 1 5CC09976
P 5800 3150
F 0 "Q1" H 6005 3196 50  0000 L CNN
F 1 "2N7000" H 6005 3105 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 6000 3075 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 5800 3150 50  0001 L CNN
	1    5800 3150
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-814 U1
U 1 1 5D68DC0F
P 5000 3050
F 0 "U1" H 5000 3375 50  0000 C CNN
F 1 "LTV-814" H 5000 3284 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 4800 2850 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0013/S_110_LTV-814%20824%20844%20(M,%20S,%20S-TA,%20S-TA1,%20S-TP)%20Series.pdf" H 5025 3050 50  0001 L CNN
	1    5000 3050
	1    0    0    -1  
$EndComp
Connection ~ 5300 3150
Wire Wire Line
	3750 2900 4700 2900
Wire Wire Line
	4300 3200 4350 3200
$Comp
L Device:C C1
U 1 1 5D68F2E2
P 4150 3400
F 0 "C1" V 4200 3300 50  0000 C CNN
F 1 "3.3nF" V 4200 3550 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4188 3250 50  0001 C CNN
F 3 "~" H 4150 3400 50  0001 C CNN
	1    4150 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3400 3950 3400
Wire Wire Line
	3950 3400 3950 3200
Connection ~ 3950 3200
Wire Wire Line
	3950 3200 4000 3200
Wire Wire Line
	4300 3400 4350 3400
Wire Wire Line
	4350 3400 4350 3200
Connection ~ 4350 3200
Wire Wire Line
	4350 3200 4700 3200
$EndSCHEMATC
